#version 330

layout(points) in;
layout(triangle_strip, max_vertices = 3) out;
out vec3 fColor;

uniform mat4 u_View;
uniform mat4 u_Proj;

void main() {
    gl_Position = u_Proj * u_View * gl_in[0].gl_Position;
    fColor = vec3(1., 0., 0.);
    EmitVertex();

    gl_Position = u_Proj * u_View * (gl_in[0].gl_Position + vec4(0.5f, 0., 0., 0.));
    fColor = vec3(0., 1., 0.);
    EmitVertex();

    gl_Position = u_Proj * u_View * (gl_in[0].gl_Position + vec4(0.f, .5, 0., 0.));
    fColor = vec3(0., 0., 1.);
    EmitVertex();
    EndPrimitive();
}