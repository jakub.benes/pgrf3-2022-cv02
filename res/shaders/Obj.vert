#version 330
in vec3 inPosition;
uniform mat4 u_View;
uniform mat4 u_Proj;
uniform mat4 u_Model;

void main() {
    gl_Position = u_Proj * u_View * u_Model * vec4(inPosition, 1.);
}