#version 410

layout (vertices = 3) out;

//layout(location = 1) in vec3 inColor[];
layout(location = 1) out vec3 outColor[];

void main() {
    if(gl_InvocationID == 0) {
        gl_TessLevelInner[0] = 1.;
        gl_TessLevelOuter[0] = 4.;
        gl_TessLevelOuter[1] = 1.;
        gl_TessLevelOuter[2] = 1.;
    }

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    if(gl_InvocationID == 0)
        outColor[gl_InvocationID] = vec3(1., 0., 0.);
    if(gl_InvocationID == 1)
        outColor[gl_InvocationID] = vec3(0., 1., 0.);
    if(gl_InvocationID == 2)
        outColor[gl_InvocationID] = vec3(0., 0., 1.);
}